var input = 'R2, L1, R2, R1, R1, L3, R3, L5, L5, L2, L1, R4, R1, R3, L5, L5, R3, L4, L4, R5, R4, R3, L1, L2, R5, R4, L2, R1, R4, R4, L2, L1, L1, R190, R3, L4, R52, R5, R3, L5, R3, R2, R1, L5, L5, L4, R2, L3, R3, L1, L3, R5, L3, L4, R3, R77, R3, L2, R189, R4, R2, L2, R2, L1, R5, R4, R4, R2, L2, L2, L5, L1, R1, R2, L3, L4, L5, R1, L1, L2, L2, R2, L3, R3, L4, L1, L5, L4, L4, R3, R5, L2, R4, R5, R3, L2, L2, L4, L2, R2, L5, L4, R3, R1, L2, R2, R4, L1, L4, L4, L2, R2, L4, L1, L1, R4, L1, L3, L2, L2, L5, R5, R2, R5, L1, L5, R2, R4, R4, L2, R5, L5, R5, R5, L4, R2, R1, R1, R3, L3, L3, L4, L3, L2, L2, L2, R2, L1, L3, R2, R5, R5, L4, R3, L3, L4, R2, L5, R5';


var commands = input.split(', ');

var x = 0;
var y = 1;

var loc = [0,0];
var facing = 'N';

var locationsVisited = [];

arrayToString = function(arr) {
  return '' + arr[0] + ',' + arr[1];
};

moveLoc = function(steps) {
  for(step = 0; step < steps; step++) {
    if(locationsVisited.indexOf(arrayToString(loc)) < 0) {
      locationsVisited.push(arrayToString(loc));
      console.log(locationsVisited);
    } else {
      console.log(locationsVisited);
      console.log( 'you visited ' + loc + ' twice!');
      return false;
    }

    if(facing == 'N') {
      loc[y] += 1;
      console.log('moving north ' + 1 + ' steps.');
    }
    else if(facing == 'E') {
      loc[x] += 1;
      console.log('moving east ' + 1 + ' steps.');
    }
    else if(facing == 'S') {
      loc[y] -= 1;
      console.log('moving south ' + 1 + ' steps.');
    }
    else {
      loc[x] -= 1;
      console.log('moving west ' + 1 + ' steps.');
    }
  }

  return true;
};

console.log(commands);
for(i = 0; i < commands.length; i++) {
  console.log('Command: ' + commands[i]);

  var direction = commands[i][0];
  var steps = Number(commands[i].substr(1,commands[i].length - 1));
  if(facing == 'N') {
    facing = direction == 'R' ? 'E' : 'W';
  }
  else if(facing == 'E') {
    facing = direction == 'R' ? 'S' : 'N';
  }
  else if(facing == 'S') {
    facing = direction == 'R' ? 'W' : 'E';
  }
  else {
    facing = direction == 'R' ? 'N' : 'S';
  }
  if(!moveLoc(steps)) {
    break;
  }
}

console.log('we are at: ' + loc);
console.log('blocks away: ' + (Math.abs(loc[x]) + Math.abs(loc[y])));
