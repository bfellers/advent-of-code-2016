fs = require('fs');
var lines;

var possibleTriangles = 0;

function compareNumbers(a,b) {
  return a > b;
}

function getTriangles(input) {
  var triangles = [];
  for(var i = 0; i < input.length; i++) {
    var sides = input[i].trim('\n').split(' ');
    var numSides = [];
    for(var x = 0; x < sides.length; x++) {
      if(!isNaN(sides[x]) && Number(sides[x]) !== 0) {
        numSides.push(Number(sides[x]));
      }
    }
    triangles.push(numSides);
  }

  var newTriangles =[];
  var triangle = [];
  for(var column = 0; column < 3; column++) {
    for(var v = 0; v < triangles.length; v++) {
      triangle.push(triangles[v][column]);
      if(triangle.length > 2) {
        newTriangles.push(triangle);
        triangle = [];
      }
    }
  }
  newTriangles.push(triangle);
  return newTriangles;
}

function findTriangles(input) {
  var triangles = getTriangles(input);
  for(var i = 0; i < triangles.length; i++) {
    var numSides = triangles[i];
    numSides.sort(compareNumbers);
    if((numSides[0] + numSides[1]) > numSides[2]) {
      console.log('GOOD: ' + numSides);
      possibleTriangles++;
    }
    else {
      console.log('BAD:  ' + numSides);
    }
  }
  console.log(possibleTriangles);
}

fs.readFile('input.txt', 'utf8', function(err,data) {
  if(err) {

  }
  else {
    lines = data.split('\n');
    lines.pop();
    findTriangles(lines);
  }
});
