var fs = require('fs');

function run() {
  fs.readFile('input.txt', 'utf8', function(err,data) {
    if(err) {

    }
    else {
      parseInput(data);
    }
  });
}

function sorter(a,b) {
  if(a.count === b.count) {
    if(a.letter > b.letter) {
      return 1;
    } else {
      return -1;
    }
  }
  return b.count - a.count;
}

function genCheckSum(input) {
  var checkSum = '';
  var lettersCount = [];
  var lettersToCheck = 'abcdefghijklmnopqrstuvwxyz';
  var inputArray = Array.from(input);
  console.log(input);

  for(var i = 0; i < lettersToCheck.length; i++){
    if(inputArray.indexOf(lettersToCheck[i]) > -1) {
      var letter = lettersToCheck[i];
      var data = {};
      data.letter = letter;
      data.count = 0;
      while(inputArray.indexOf(lettersToCheck[i]) > -1) {
        inputArray.splice(inputArray.indexOf(lettersToCheck[i]), 1);
        data.count++;
      }
      if(typeof data !== 'undefined' && data.count > 0) {
        lettersCount.push(data);
      }
    }
  }
  lettersCount.sort(sorter);
  lettersCount.sort(sorter);
  console.log(lettersCount);

  for(var q = 0; q < 5; q++) {
    checkSum += lettersCount[q].letter;
  }

  return checkSum;
}

function parseInput(input) {
  var split = input.split('\n');
  // remove last blank entry
  split.pop();

  var sectorSum = 0;
  for(i = 0; i < split.length; i++) {
    var toCount = split[i].substr(0, split[i].lastIndexOf('-'));
    var sectorId = split[i].substr(split[i].lastIndexOf('-') + 1).split('[')[0];
    var checkSum = split[i].substr(split[i].indexOf('[') + 1).split(']')[0];
    var ourCheckSum = genCheckSum(toCount);

    if(ourCheckSum === checkSum) {
      sectorSum += Number(sectorId);
    }

    console.log(split[i]);
    console.log(' checking: ' + toCount);
    console.log(' sectorid: ' + sectorId);
    console.log(' checksum: ' + checkSum);
    console.log(' ourchecksum: ' + ourCheckSum);
  }
  console.log('');
  console.log('sectorsum: ' + sectorSum);
}



run();
