var fs = require('fs');

function run() {
  fs.readFile('input.txt', 'utf8', function(err,data) {
    if(err) {

    }
    else {
      parseInput(data);
    }
  });
}

function shiftLetter(letter, shift) {
  if(letter.charCodeAt() + shift > 'z'.charCodeAt()) {
    return String.fromCharCode('a'.charCodeAt() + (letter.charCodeAt() + shift) - 'z'.charCodeAt() - 1);
  }
  return String.fromCharCode(letter.charCodeAt() + shift);
}

function shiftWords(words, shift) {
  var shifted = '';
  for(var i = 0; i < words.length; i ++) {
    if(words[i] === '-') {
      shifted += ' ';
    }
    else {
      shifted += shiftLetter(words[i], shift);
    }
  }
  return shifted;
}


function parseInput(input) {
  var split = input.split('\n');
  // remove last blank entry
  split.pop();

  var sector;
  for(i = 0; i < split.length; i++) {
    var toCount = split[i].substr(0, split[i].lastIndexOf('-'));
    var sectorId = split[i].substr(split[i].lastIndexOf('-') + 1).split('[')[0];
    var toShiftBy = Number(sectorId) % 26;
    var shifted = shiftWords(toCount, toShiftBy);

    if(shifted.indexOf('north') > -1) {
    console.log(split[i]);
    console.log(' checking: ' + toCount);
    console.log(' sectorid: ' + sectorId);
    console.log(' toShiftBy: ' + toShiftBy);
    console.log(' shifted: ' + shifted);

      console.log('found');
    }
  }
  console.log('');
  console.log('north pole objects: ' + sector);
}



run();
