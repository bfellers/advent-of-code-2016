var fs = require('fs');
var md5 = require('md5');

function run() {
  fs.readFile('input.txt', 'utf8', function(err,data) {
    if(err) {

    }
    else {
      parseInput(data);
    }
  });
}

function findPassword(input) {
  var password = '';
  var index = 0;
  while(password.length < 8) {
    var hash = md5(input + index);
    if(hash.substr(0,5) == '00000') {
      password += hash[5];
      console.log(hash);
    }
    index++;
  }
  return password;
}

function parseInput(input) {
  var split = input.split('\n');
  // remove last blank entry
  split.pop();

  for(i = 0; i < split.length; i++) {
    console.log (findPassword(split[i]));
  }
}



run();
