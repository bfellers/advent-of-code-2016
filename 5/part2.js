var fs = require('fs');
var md5 = require('md5');

function run() {
  fs.readFile('input.txt', 'utf8', function(err,data) {
    if(err) {

    }
    else {
      parseInput(data);
    }
  });
}

function findPassword(input) {
  var passwordArray = [null, null, null, null, null, null, null, null];
  var password = '';
  var index = 0;
  while(passwordArray.indexOf(null) > -1) {
    var hash = md5(input + index);
    if(hash.substr(0,5) == '00000') {
      if(!isNaN(hash[5]) && passwordArray[hash[5]] === null) {
        passwordArray[hash[5]] = hash[6];
      }
      console.log(hash);
    }
    index++;
  }
  password = passwordArray.join('');
  return password;
}

function parseInput(input) {
  var split = input.split('\n');
  // remove last blank entry
  split.pop();

  for(i = 0; i < split.length; i++) {
    console.log (findPassword(split[i]));
  }
}



run();
