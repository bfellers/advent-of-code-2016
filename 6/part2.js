var fs = require('fs');

function run() {
  fs.readFile('input.txt', 'utf8', function(err,data) {
    if(err) {

    }
    else {
      parseInput(data);
    }
  });
}


function findMostCharacters(chars) {
  var leastUsedCount = 100000;
  var leastUsedChar = null;
  for(var i = 0; i < chars.length; i++) {
    var count = 0;
    var currentChar = chars[i];
    for(var v = 0; v < chars.length; v++) {
      if(currentChar == chars[v]) {
        count++;
      }
    }
    if (count < leastUsedCount) {
      leastUsedCount = count;
      leastUsedChar = currentChar;
    }
  }
  return leastUsedChar;
}


function parseInput(input) {
  var split = input.split('\n');
  // remove last blank entry
  split.pop();

  letters = [];
  fixed = '';
  for(i = 0; i < split.length; i++) {
    // setup initial arrays
    if(i === 0) {
      for(c = 0; c < split[i].length; c++) {
        letters.push([]);
      }
    }

    // add chacters to each character array
    for(c = 0; c < split[i].length; c++) {
      letters[c].push(split[i][c]);
    }
  }

  for(i = 0; i < letters.length; i++) {
    fixed += findMostCharacters(letters[i]);
  }
  console.log(fixed);
}




run();
