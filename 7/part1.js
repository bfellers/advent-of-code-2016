var fs = require('fs');

function run() {
  fs.readFile('input.txt', 'utf8', function(err,data) {
    if(err) {

    }
    else {
      parseInput(data);
    }
  });
}


function doesItSupportTLS(section) {
  var supportsTls = false;
  var insideBrackets = false;
  for(var i = 0; i < section.length; i++) {
    if(section[i] === '[') {
      insideBrackets = true;
    }
    if(section[i] === ']') {
      insideBrackets = false;
    }
    if(section[i] != section[i + 1] &&
       (section[i] + section[i + 1]) == (section[i + 3] + section[i + 2])) {
         if(insideBrackets) {
           return false;
         }
         else {
           supportsTls = true;
         }
    }
  }
  return supportsTls;
}

function parseInput(input) {
  var split = input.split('\n');
  // remove last blank entry
  split.pop();

  var supportTlsCount = 0;
  for(var i = 0; i < split.length; i++) {
    if(doesItSupportTLS((split[i]))) {
      //console.log(split[i] + ' - YES');
      supportTlsCount++;
    }
    else {
      //console.log(split[i] + ' - NO');
    }
  }
  console.log('number of ips supporting tls ' + supportTlsCount);
}



run();
