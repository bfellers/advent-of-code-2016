var fs = require('fs');

function run() {
  fs.readFile('input.txt', 'utf8', function(err,data) {
    if(err) {

    }
    else {
      parseInput(data);
    }
  });
}


function doesItSupportSSL(section) {
  var insideBrackets = false;
  for(var i = 0; i < section.length; i++) {
    if(section[i] === '[') {
      insideBrackets = true;
    }
    if(section[i] === ']') {
      insideBrackets = false;
    }
    if(section[i] != section[i + 1] &&
       section[i] == section[i + 2]) {
         var inside = false;
        for(var x = 0; x < section.length; x++) {
          if(section[x] === '[') {
            inside = true;
          }
          if(section[x] === ']') {
            inside = false;
          }
          if(insideBrackets != inside &&
            section[x] != section[x + 1] &&
             section[x] == section[x + 2] &&
             section[i] == section[x + 1] && section[i + 1] == section[x]) {
               return true;
             }
        }
    }
  }
  return false;
}

function parseInput(input) {
  var split = input.split('\n');
  // remove last blank entry
  split.pop();

  var supportTlsCount = 0;
  for(var i = 0; i < split.length; i++) {
    if(doesItSupportSSL((split[i]))) {
      //console.log(split[i] + ' - YES');
      supportTlsCount++;
    }
    else {
      //console.log(split[i] + ' - NO');
    }
  }
  console.log('number of ips supporting tls ' + supportTlsCount);
}



run();
