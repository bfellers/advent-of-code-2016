var fs = require('fs');

function run() {
  fs.readFile('input.txt', 'utf8', function(err,data) {
    if(err) {

    }
    else {
      parseInput(data);
    }
  });
}

function parseLine(line) {
  var split = line.split(' ');
  var term = split[0];

  if(term === 'rect') {
    drawRect(split[1]);
  }
  if(term === 'rotate') {
    if(split[1] === 'column') {
      //rotateColumn(split[2], split[4]);
    }
    else {
      rotateRow(split[2], split[4]);
    }
  }
}

function rotateColumn(column, amount) {
  var col = column.split('=')[1];
  var amt = Number(amount);

  var ogScreen = JSON.parse(JSON.stringify(screen));

  for(var x = 0; x < screen.length; x++) {
    if(typeof ogScreen[x - amt] ) {
      screen[x] = ogScreen[screen.length - amt];
    }
    else {
      screen[x] = ogScreen[x - amt];
    }
  }
}

function rotateRow(row, amount) {
  var r = row.split('=')[1];
  var amt = Number(amount);

  var ogScreen = JSON.parse(JSON.stringify(screen));


  for(var y = screen[r].length - 1; y >= 0; y--) {
    if(typeof ogScreen[r][y - amt] === 'undefined') {
      screen[r][y] = ogScreen[r][screen[r].length - amt - y];
    }
    else {
      screen[r][y] = ogScreen[r][y - amt];
    }
  }
}

function drawRect(dimensions) {
  var splitDim = dimensions.split('x');

  for(var y = 0; y < splitDim[1]; y++) {
    for(var x = 0; x < splitDim[0]; x++) {
      screen[y][x] = '#';
    }
  }
}

function parseInput(input) {
  var split = input.split('\n');
  // remove last blank entry
  split.pop();

  for(var i = 0; i < split.length; i++) {
    parseLine(split[i]);
    console.log(split[i]);
    printScreen();
  }
}

var screen = [];
for(var y = 0; y < 6; y++) {
  for(var x = 0; x < 50; x++) {
    if(typeof screen[y] === 'undefined') {
      screen[y] = [];
    }
    screen[y].push('.');
  }
}

function printScreen() {
  for(var y = 0; y < 6; y++) {
    var line = '';
    for(var x = 0; x < 50; x++) {
      line += screen[y][x];
    }
    console.log(line);
  }
}


run();
